// INTRODUCTION TO JSON (JavaScript Object Notation)

/*
	JSON -> is a 'data representation format' similar to XML and YAML

	Uses of JSON Data Format:
	=> is commonly used for API and Configuration.
	=> is also used in serializing different data types into 'bytes'.

	Serialization:
	=> is the process of converting data into a series of 'bytes' for easier transmission/transfer of information

	'byte' -> is a unit of data, eight binary digits (1 and 0), that is used to represent a character(letters, numbers, typographic symbols).

	Benefits of Serialization:
	=> once a piece of data/information has been serialized, they become 'lightweight'. It becomes a lot easier to transfer or transmit over a network or connection.
*/


// STRUCTURE OF A JSON FORMAT

/*
	JSON data is similar to the structure of a JS Object.
	JS Objects are NOT to be confused with JSON.

	Syntax of JSON Data:
	=> JSON uses double quotes ("") only for its property names.
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}

	Example:
	{
		"city": "Quezon City",
		"province": "Metro Manila",
		"country": "Philippines"
	}
*/


// JSON DATA TYPES
/*
	JSON accepts the following values:
	1. Strings => "Hello", "Hello World"
	2. Number => 10, 1.5, -30, 1.2e10
	3. Boolean => True or False
	4. null => empty/null
	5. Array => []
	6. Object => { key: value }

	NOTE: JSON Files is also commonly used to store a series of information.

		Createa a file called employees.json to store multiple information about the company's employees.
*/

	// REMEMBER: Anything that you write in JSON is a VALID JavaScript. The syntaxes will be recognized properly by JS.

	// Example 1:
	let employees = [
		{
			"name": "Thonie Fernandez",
			"department": "Instructor",
			"yearEmployed": "2020",
			"ratings": 5.0
		},
		{
			"name": "Charles Quimpo",
			"department": "Instructor",
			"yearEmployed": "2010",
			"ratings": 5.0
		},
		{
			"name": "Martin Miguel",
			"department": "Instructor",
			"yearEmployed": "2019",
			"ratings": 4.0
		},
		{
			"name": "Alvin Estiva",
			"department": "Instructor",
			"yearEmployed": "2020",
			"ratings": 5.0
		}
	];

	console.log(employees);

	// Example 2:
	let user = {
		"name": "Kyle",
		"favoriteNumber": 7,
		"isProgrammer": true,
		"hobbies": [
			"Weightlifting",
			"Reading Comics",
			"Playing the Guitar",
		],
		"friends": [
			{
				"name": "Robin",
				"isProgrammer": true
			},
			{
				"name": "Daniel",
				"isProgrammer": false
			},
			{
				"name": "Oswald",
				"isProgrammer": true
			}
		]
	};

	console.log(user);

/*
	JSON is commonly used to describe an application/project. If you are going to create a node project, it usually comes with package.json file.

	"package.json" -> is considered as the "heart" of any application which is used to describe the overall structure of an application.
*/
	let application = `{
		"name": "javascript server",
		"version": "1.0",
		"description": "server side application done using javascript and node js",
		"main": "index.js",
		"scripts": {
			"start": "node index.js"
		},
		"keywords": [
			"server",
			"node",
			"backend"
		],
		"author": "John Smith",
		"license": "ISC"
	}`;

	console.log(application);
	console.log(typeof application); //object data type is converted into a string

/*
	When dealing with JSON, you will usually retrieve this data format in all strings. In order to emulate, we will use backticks.

	Whenever you're dealing with JSON data that are in all string, the data you received is NOT very usable. 
*/
	console.log(application.name); // undefined, can't be accessed because the retrieved data is a string.


// DIFFERENT JSON METHODS
/*
	The JSON object contains method for parsing and converting data into 'stringified' JSON.

	1. Converting JSON data to the 'stringified version':
		JSON.stringify() => converts a JSON object into all strings.
		=> this is commonly used when sending HTTP requests where information is required to be sent and received in a "stringified" JSON format.

*/
	console.log(employees);
	let jsonString = JSON.stringify(employees);
	console.log(jsonString);

/*
	2. Converting JSON Strings into JSON Objects:
		JSON.parse() -> converts the stringified object into a JS object.
*/
	let jsonObject = JSON.parse(jsonString);
	console.log(jsonObject);

	// Upon converting a stringed JSON to JS Object, you may now use and access the data stored.
	console.log(jsonObject[0].name); //Thonie Fernandez
	console.log(JSON.parse(application).name); //javascript server

